# README #

Empty Project - Yet another HTML5BP + Grunt starter project.

### Simplify new project creation ###

* This is a simple empty starter project for flat file sites. It uses HTML5BP with a few useful Grunt tasks. Note that it does not track HTML5BP's repo in any way, so if you need to update HTML5BP you'll have to do it manually.

### How do I get set up? ###

* In your terminal, go to the root directory of your project. Type 'npm install' to install the grunt dependencies. Once that completes, simply run the command 'grunt' from your terminal.
* Be sure to edit only the files in 'www-dev'. 'www-root' is the destination directory for the Grunt tasks, and is what you should deploy.
* Once your Grunt task is running, browse to localhost:8000 to see the locally hosted project.

### Grunt tasks include: ###
* grunt-contrib-concat
* grunt-contrib-uglify
* grunt-contrib-jshint
* grunt-contrib-imagemin
* grunt-contrib-watch
* grunt-contrib-sass
* grunt-contrib-connect
* grunt-autoprefixer
* grunt-contrib-htmlmin
* grunt-responsive-images
* grunt-newer
* grunt-notify

### Contribution guidelines ###

* Feel free to submit pull requests, but I somehow doubt anyone will be particularly moved to contribute to this as it's primarily a personal project.

### Who do I talk to? ###

* Feel free to contact tim@timrourke.com with any questions or comments.