module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        concat: {
            dist: {
                src: [
                    'www-dev/js/libs/*.js',
                    'www-dev/js/*.js'
                ],
                dest: 'www-root/js/build/global.js',
            }
        },
        uglify: {
            build: {
                src: 'www-root/js/build/global.js',
                dest: 'www-root/js/build/global.min.js'
            }
        },
        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: 'www-dev/img-build/',
                    src: ['**/*.{png,jpg,gif}'],
                    dest: 'www-root/img/'
                }]
            }
        },
        responsive_images: {
            myTask: {
                options: {
                    quality: 60,
                    createNoScaledImage: true,
                    sizes: [{
                        width: '100%',
                        rename: false,
                        quality: 60
                    },{
                        width: 320
                    },{
                        width: 640
                    },{
                        width: 1024
                    }]
            },
            files: [{
                expand: true,
                cwd: 'www-dev/img/',
                src: ['**/*.{jpg,gif,png}'],
                dest: 'www-dev/img-build/'
                }]
            }
        },
        sass: {
            dist: {
                options: {
                    style: 'compressed'
                },
                files: {
                    'www-root/css/style.css': 'www-dev/scss/style.scss',
                    'www-root/css/style-ie.css': 'www-dev/scss/style-ie.scss'
                }
            }
        },
        autoprefixer: {
            options: {
                browsers: ['last 8 versions', 'ie 8', 'ie 9']
            },
            no_dest: {
                src: 'www-root/css/*.css'
            }
        },
        watch: {
            images: {
                files: ['www-dev/img/**/*.*'],
                tasks: ['responsive_images','newer:imagemin'],
                options: {
                    spawn: false,
                    livereload: 35729
                }
            },
            scripts: {
                files: ['www-dev/js/*.js'],
                tasks: ['concat', 'uglify'],
                options: {
                    spawn: false,
                    livereload: true
                },
            },
            css: {
                files: ['www-dev/scss/*.scss'],
                tasks: ['sass', 'autoprefixer'],
                options: {
                    spawn: false,
                    livereload: true
                }
            },
            html: {
                files: ['www-dev/**/*.html'],
                tasks: ['htmlmin'],
                options: {
                    livereload: true
                }
            },
            php: {
                files: ['www-dev/**/*.php'],
                options: {
                    spawn: false,
                    livereload: 35729
                }
            }
        },
        jshint: {
            beforeconcat: ['www-dev/js/global.js'],
            options: {
                globals: {
                    jQuery: true
                }
            }
        },
        htmlmin: {
            dist: {
                options: {
                    keepClosingSlash: true,
                    removeComments: true,
                    collapseWhitespace: true
                },
                files: [{
                    expand: true,
                    cwd: 'www-dev',
                    src: '**/*.html',
                    dest: 'www-root'
                }]
            },
            dev: {
                options: {
                    keepClosingSlash: true,
                    removeComments: true,
                    collapseWhitespace: true
                },
                files: [{
                    expand: true,
                    cwd: 'www-dev',
                    src: '**/*.html',
                    dest: 'www-root'
                }]
            }
        },
        connect: {
            server: {
                options: {
                    livereload: true,
                    base: './www-root/'
                }
            }
        }


    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-autoprefixer');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');
    grunt.loadNpmTasks('grunt-responsive-images');
    grunt.loadNpmTasks('grunt-newer');
    grunt.loadNpmTasks('grunt-notify');

    grunt.registerTask('default', ['jshint', 'concat', 'uglify', 'newer:responsive_images', 'newer:imagemin', 'sass', 'autoprefixer', 'htmlmin', 'connect', 'watch']);

};